#include "SDL/SDL.h"
#include <math.h>
#include "controller.h"

SDL_Surface *sc;

Controller::Controller(SDL_Surface *s) {
	int x, y;

	sc = s;

	mouseDown = false;
	mouseForceInvert = false;

	mouseX = 0;
	mouseY = 0;

	Uint32 color;

	for (int p = 0; p < TOTAL_PARTICLES; p++) {

		color = 0xFF000000;

		color = color | (int)(rand() % 255) << 16;
		color = color | (int)(rand() % 255) << 8;
		color = color | (int)(rand() % 255);

		x = rand() % sc->w;
		y = rand() % sc->h;
		particles[ p ] = new P();
		particles[ p ]->color = color;
		particles[ p ]->positionX = (float) x;
		particles[ p ]->positionY = (float) y;
		particles[ p ]->velocityX = 0.0f;
		particles[ p ]->velocityY = 0.0f;
	}
}

Controller::~Controller() {
	for (int p = 0; p < TOTAL_PARTICLES; p++) {
		delete particles[p];
	}
}

void Controller::setMouseForceInverted(bool inverted) {
	mouseForceInvert = inverted;
}

void Controller::setMouseDown(bool down) {
	mouseDown = down;
}


void Controller::setMousePosition(int x, int y) {
	mouseX = x;
	mouseY = y;
}

void Controller::draw() {
	SDL_LockSurface(sc);
	Uint32 *pixels = (Uint32 *)sc->pixels;
	//Go through particles
	for (int particle = 0; particle < TOTAL_PARTICLES; particle++) {
		int p = (((int)particles[particle]->positionY * sc->w) + (int)particles[particle]->positionX);
		pixels[p] = particles[particle]->color;
	}
	SDL_UnlockSurface(sc);
}

void Controller::step() {

	if (INTER_PARTICLE_FORCE != 0) {
		for (int outer = 0; outer < TOTAL_PARTICLES; outer++) {
			for (int inner = outer + 1; inner < TOTAL_PARTICLES; inner++) {
				applyForceFromParticle(particles[outer], particles[inner], INTER_PARTICLE_FORCE);
			}
		}
	}

	if (mouseDown) {
		int force = mouseForceInvert ? -POINTER_FORCE : POINTER_FORCE;
		for (int particle = 0; particle < TOTAL_PARTICLES; particle++) {
			applyForce(particles[particle], mouseX, mouseY, force);
		}
	} else {
		for (int particle = 0; particle < TOTAL_PARTICLES; particle++) {
			move(particles[particle], 1);
		}
	}

	for (int p = 0; p < TOTAL_PARTICLES; p++) {
		sanitizePositions(particles[p]);
	}
}

void move(P *particle, int divisor) {
	particle->positionX += particle->velocityX / divisor;
	particle->positionY += particle->velocityY / divisor;
}

void sanitizePositions(P *particle) {
	if (particle->positionX > sc->w) {
		particle->positionX -= particle->positionX - sc->w;
		particle->velocityX = -particle->velocityX / 2;
	}

	if (particle->positionY > sc->h) {
		particle->positionY -= particle->positionY - sc->h;
		particle->velocityY = -particle->velocityY / 2;
	}

	if (particle->positionX < 0) {
		particle->positionX = -particle->positionX;
		particle->velocityX = -particle->velocityX / 2;
	}

	if (particle->positionY < 0) {
		particle->positionY = -particle->positionY;
		particle->velocityY = -particle->velocityY / 2;
	}

	if (particle->positionX >= sc->w) {
		particle->positionX--;
	}

	if (particle->positionY >= sc->h) {
		particle->positionY--;
	}
}

void applyForce(P *particle, float forcePositionX, float forcePositionY, int attraction) {
	float d = pow(particle->positionX - forcePositionX, 2.0) + pow(particle->positionY - forcePositionY, 2.0);
	float vlength;
	int steps = 1;

	if (d < 5) {
		steps = 20;
	} else if(d < 10) {
		steps = 10;
	} else if(d < 20) {
		steps = 5;
	} else if(d < 40) {
		steps = 2;
	}

	for (int step = 0; step < steps; step++) {
		d = pow(particle->positionX - forcePositionX, 2.0) + pow(particle->positionY - forcePositionY, 2.0);
		if (d < 5) {
			steps = 20;
		} else if(d < 10) {
			steps = 10;
		} else if(d < 20) {
			steps = 5;
		} else if(d < 40) {
			steps = 2;
		} else {
			steps = 1;
		}
		vlength = sqrt(d);
		particle->velocityX += ((particle->positionX - forcePositionX) / vlength * ((attraction) / d)) / steps;
		particle->velocityY += ((particle->positionY - forcePositionY) / vlength * ((attraction) / d)) / steps;
		move(particle, steps);
	}

}

void applyForceFromParticle(P *particle1, P *particle2, int attraction) {
	float d = pow(particle1->positionX - particle2->positionX, 2.0) + pow(particle1->positionY - particle2->positionY, 2.0);
	float vlength = sqrt(d);

	if (vlength < 1) vlength = 1;
	if (d < 5) d = 5;

	float forceX = (particle1->positionX - particle2->positionX) / vlength * (attraction / d);
	float forceY = (particle1->positionY - particle2->positionY) / vlength * (attraction / d);

	particle1->velocityX += forceX;
	particle1->velocityY += forceY;
	particle2->velocityX -= forceX;
	particle2->velocityY -= forceY;
}
