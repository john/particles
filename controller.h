const int INTER_PARTICLE_FORCE = -1.0;
const int POINTER_FORCE = -100;

const int TOTAL_PARTICLES = 1000;

struct P {
	double positionX, positionY, velocityX, velocityY;
	float distance;
	Uint32 color;
};

class Controller {
	private:

	P *particles[TOTAL_PARTICLES];

	int mouseX, mouseY;
	bool mouseDown, mouseForceInvert;

	public:
	//Constructor
	Controller(SDL_Surface *s);
	//Destructor
	~Controller();

	void setMouseDown(bool down);
	void setMouseForceInverted(bool inverted);
	void setMousePosition(int x, int y);

	//Shows the particle
	void draw();
	void step();
};

void move(P *particle, int divisor);
void applyForce(P *particle, float forcePositionX, float forcePositionY, int attraction);
void applyForceFromParticle(P *particle1, P *particle2, int attraction);
void sanitizePositions(P *particle);
