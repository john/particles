#include "SDL/SDL.h"
#include <string>
#include <cstdlib>
#include <sstream>

#include "timer.h"
#include "window.h"
#include "controller.h"

using namespace std;

const int FRAMES_PER_SECOND = 60;

SDL_Event event;

bool init() {
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
		return false;
	}

	SDL_WM_SetCaption("Particle Test", NULL);

	srand(SDL_GetTicks());

	return true;
}

void clean_up() {
	SDL_Quit();
}

int main(int argc, char* args[]) {
	bool quit = false;

	Window myWindow;

	//If the window failed
	if( myWindow.error() == true )
	{
		return 1;
	}

	Controller c = Controller(myWindow.getScreen());

	Timer fps;
	Timer fps2;
	Timer update;

	if (init() == false) {
		return 1;
	}

	update.start();
	fps.start();
	int frame = 0;

	while (quit == false) {
		fps2.start();

		while (SDL_PollEvent(&event)) {

			myWindow.handle_events(event);

			if ((event.type == SDL_KEYDOWN) && (event.key.keysym.sym == SDLK_ESCAPE)) {
				quit = true;
			}

			if (event.type == SDL_QUIT) {
				quit = true;
			}

			if (event.type == SDL_MOUSEBUTTONDOWN) {
				c.setMouseForceInverted(event.button.button == SDL_BUTTON_RIGHT);
				c.setMouseDown(true);
				c.setMousePosition(event.button.x, event.button.y);
			}

			if (event.type == SDL_MOUSEBUTTONUP) {
				c.setMouseDown(false);
			}

			if (event.type == SDL_MOUSEMOTION) {
				c.setMousePosition(event.button.x, event.button.y);
			}
		}

		if( myWindow.error() == true )
		{
			return 1;
		}

		myWindow.clear();
		c.step();
		c.draw();

		myWindow.flip();

		frame++;

		if (fps2.get_ticks() < 1000 / FRAMES_PER_SECOND) {
			SDL_Delay((1000 / FRAMES_PER_SECOND) - fps2.get_ticks());
		}

		if (update.get_ticks() > 1000) {
			std::stringstream caption;

			caption << "Average Frames Per Second: " << 1000 / fps2.get_ticks();

			SDL_WM_SetCaption(caption.str().c_str(), NULL);

			update.start();
		}

	}

	clean_up();

	return 0;
}
