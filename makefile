CC=g++
CFLAGS=-pipe -Wall
GCCFLAGS=-I. -lSDL -lm -lstdc++
DEPS=window.h timer.h controller.h
OBJ=window.o timer.o controller.o main.o

%.o: %.cpp $(DEPS)
		$(CC) -c $(CFLAGS) -o $@ $<

particles: $(OBJ)
		gcc -o $@ $^ $(CFLAGS) $(GCCFLAGS)
		make clean_obj

particles_O3: CFLAGS += -O3 -fomit-frame-pointer
particles_O3: particles

particles_pgo_gen: CFLAGS += -O3 -fomit-frame-pointer -fprofile-generate
particles_pgo_gen: $(OBJ)
		gcc -o $@ $^ $(CFLAGS) $(GCCFLAGS)
		make clean_obj

particles_pgo_use: CFLAGS += -O3 -fomit-frame-pointer -fprofile-use
particles_pgo_use: $(OBJ)
		gcc -o particles_pgo $^ $(CFLAGS) $(GCCFLAGS)
		make clean_obj
		make clean_profile_data
		make clean_bin

particles_pgo:
		make particles_pgo_gen
		./particles_pgo_gen
		make particles_pgo_use

clean_obj:
		rm -fv *.o

clean_bin:
		rm -fv particles_pgo_gen

clean_profile_data:
		rm -fv *.gcda

clean_all: clean_obj clean_bin clean_profile_data