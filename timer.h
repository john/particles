class Timer {
	private:

	int startTicks;
	bool started;

	public:

	Timer();

	void start();
	void stop();

	int get_ticks();

	bool is_started();
};
