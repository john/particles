const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;
const int SCREEN_BPP = 32;

class Window {
	private:
	//Whether the window is windowed or not
	bool windowed;

	SDL_Surface *screen;

	//Whether the window is fine
	bool windowOK;

	public:
	//Constructor
	Window();

	SDL_Surface * getScreen();

	void clear();

	void flip();

	//Handle window events
	void handle_events(SDL_Event event);

	//Turn fullscreen on/off
	void toggle_fullscreen();

	//Check if anything's wrong with the window
	bool error();
};
